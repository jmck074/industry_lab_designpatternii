package ictgradschool.industry.lab_designpatternii.ex02.gui;

import ictgradschool.industry.lab_designpatternii.ex02.model.Course;
import ictgradschool.industry.lab_designpatternii.ex02.model.CourseListener;


public class DistributionPanelAdapter implements CourseListener{
	DistributionPanel distPanel;



	public DistributionPanelAdapter(DistributionPanel distPanel){
		this.distPanel = distPanel;

		}


	@Override
	public void courseHasChanged(Course course) {

distPanel.repaint();
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */
}
