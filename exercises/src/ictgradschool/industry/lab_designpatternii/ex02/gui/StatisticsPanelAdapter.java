package ictgradschool.industry.lab_designpatternii.ex02.gui;

import ictgradschool.industry.lab_designpatternii.ex02.model.Course;
import ictgradschool.industry.lab_designpatternii.ex02.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener{
	StatisticsPanel stats;

	public StatisticsPanelAdapter(StatisticsPanel stats){
		this.stats = stats;
	}
	@Override
	public void courseHasChanged(Course course) {
		stats.repaint();
	}

	/**********************************************************************
	 * YOUR CODE HERE
	 */

	/**
	 * Creates a StatisticsPanel object.
	 *
	 * @param course
	 */

}
