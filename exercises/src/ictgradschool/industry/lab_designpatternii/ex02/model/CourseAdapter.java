package ictgradschool.industry.lab_designpatternii.ex02.model;


import ictgradschool.industry.lab_designpatternii.ex02.gui.DistributionPanelAdapter;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {
	private Course course;


	public CourseAdapter(Course course){
		this.course = course;
	}



	@Override
	public void courseHasChanged(Course course) {

        fireTableDataChanged();

	}


	@Override
	public int getRowCount() {
        

	    return course.size();

	}

	@Override
	public int getColumnCount() {
       //Returning 7 because I know it is seven, though not sure how to get the answer properly
	    return 7;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
 StudentResult result = course.getResultAt(rowIndex);
	    //String[] names ={"_studentId","_studentSurname","_studentForename","examMark", "testMark","assignmentMark", "overallMark" };
	    if(columnIndex==0){return result._studentID;}
	    else if(columnIndex==1){return result._studentSurname;}
	    else if(columnIndex==2){return result._studentForename;}
	    else if(columnIndex==3){return result.getAssessmentElement(StudentResult.AssessmentElement.Exam);}
	    else if(columnIndex==4){return result.getAssessmentElement(StudentResult.AssessmentElement.Test);}
	    else if(columnIndex==5){return result.getAssessmentElement(StudentResult.AssessmentElement.Assignment);}
	    else if(columnIndex==6){return result.getAssessmentElement(StudentResult.AssessmentElement.Overall);}
 return -1;
	}


	/**********************************************************************
	 * YOUR CODE HERE
	 */

}